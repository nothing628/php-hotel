<?php

require_once('connection.php');

if (!function_exists('redirectTo')) {
    function redirectTo($path, $status_code = 302)
    {
        header("Location: /$path", true, $status_code);
        die();
    }
}

if (!function_exists('checkLogin')) {
    function checkLogin()
    {
        if (!isset($_SESSION['user_id']) || !isset($_SESSION['user'])) {
            redirectTo('index.php');
        }
    }
}

if (!function_exists('ubahStatusKamar')) {
    function ubahStatusKamar($id_kamar, $status) {
        global $mysqli;

        $query = "UPDATE tbl_kamar SET status=? WHERE id=?";
        if (!($stat = $mysqli->prepare($query))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stat->bind_param('si', $status, $id_kamar);

        return $stat->execute();
    }
}

if (!function_exists('calculateHarga')) {
    function calculateHarga($id_transaksi, $tgl_checkout)
    {
        global $mysqli;
        $subtotal = 0;
        $query = "SELECT tbl_transaksi.tgl_checkin, tbl_harga.harga FROM tbl_transaksi";
        $query .= " JOIN tbl_kamar ON tbl_kamar.id = tbl_transaksi.id_kamar";
        $query .= " JOIN tbl_harga ON tbl_harga.id = tbl_kamar.id_harga";
        $query .= " WHERE tbl_transaksi.id = ? LIMIT 1";

        if (!($stat = $mysqli->prepare($query))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stat->bind_param('i', $id_transaksi);
        if ($stat->execute()) {
            $res = $stat->get_result();
            $first = $res->fetch_object();

            if ($first) {
                $harga = intval($first->harga);
                $tgl_parse_awal = date_create($first->tgl_checkin);
                $tgl_parse_akhir = date_create($tgl_checkout);
                $interval_days = date_diff($tgl_parse_akhir, $tgl_parse_awal)->d;

                $subtotal = $harga * $interval_days;
            }
        }

        return $subtotal;
    }
}
