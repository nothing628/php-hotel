# Pengenalan

Ini adalah tugas pemrograman web lanjut dengan tema perhotelan

## docker mariadb
`docker run -it --name mysql-hotel-3307 -e MYSQL_DATABASE=hotel -e MYSQL_USER=hotel -e MYSQL_PASSWORD=secret -e MYSQL_ROOT_PASSWORD=secret -p 3307:3306 -d mariadb:10`

## dump database
```
docker cp ./dump_database.sql mysql-hotel-3307:/dump.sql
mysql -p
\. dump.sql
```

