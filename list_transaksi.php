<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$data = [];
$query = "SELECT tbl_transaksi.*, tbl_kamar.nomor_kamar, tbl_pengunjung.nama_lengkap FROM tbl_transaksi";
$query .= " JOIN tbl_kamar ON tbl_transaksi.id_kamar = tbl_kamar.id";
$query .= " JOIN tbl_pengunjung ON tbl_transaksi.id_pengunjung = tbl_pengunjung.id";
$query .= " WHERE tbl_transaksi.status = 'aktif'";
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;

if ($keyword) {
    $query .= " AND status LIKE ?";
}

if (!($stmt = $mysqli->prepare($query))) {
    die("Prepare failed: ($mysqli->errno) $mysqli->error");
}

if ($keyword) {
    $new_keyword = "%$keyword%";
    $stmt->bind_param('s', $new_keyword);
}

if ($stmt->execute()) {
    $res = $stmt->get_result();

    while ($row = $res->fetch_object()) {
        $data[] = $row;
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Daftar Transaksi Aktif</h3>
    <form action="" method="get">
        <div class="row">
            <div class="input-field col s8">
                <input placeholder="Pencarian" id="keyword" type="text" class="validate" name="keyword" <?= (!$keyword) ?: "value='$keyword'" ?>>
                <label for="keyword">Pencarian</label>
            </div>
            <div class="col s4">
                <button class="waves-effect waves-light btn" type="submit">
                    <i class="material-icons left">search</i>
                    Cari
                </button>
                <a class="waves-effect waves-light btn" href="/form_transaksi.php">
                    <i class="material-icons left">create</i>
                    Tambah
                </a>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col s12">
            <table class="responsive-table striped highlight">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nomor Kamar</th>
                        <th>Nama Lengkap</th>
                        <th>Status</th>
                        <th>Jumlah Bayar</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->nomor_kamar ?></td>
                            <td>(<?= $value->id_pengunjung ?>) <?= $value->nama_lengkap ?></td>
                            <td><?= $value->status ?></td>
                            <td>Rp<?= $value->jml_bayar ?></td>
                            <td>
                                <?php if ($value->status != 'selesai') { ?>
                                    <a class="waves-effect waves-light btn btn-floating blue" href="/form_transaksi.php?id=<?= $value->id ?>">
                                        <i class="material-icons left">create</i>
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('views/footer.php');
?>