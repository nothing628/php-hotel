<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();



function getKamar()
{
    global $mysqli;
    $result = [];

    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_kamar WHERE status='kosong'"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    if ($stmt->execute()) {
        $res = $stmt->get_result();

        while ($row = $res->fetch_object()) {
            $result[] = $row;
        }
    }

    return $result;
}

function getTamu()
{
    global $mysqli;
    $result = [];

    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_pengunjung WHERE is_blocked=0"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    if ($stmt->execute()) {
        $res = $stmt->get_result();

        while ($row = $res->fetch_object()) {
            $result[] = $row;
        }
    }

    return $result;
}

$list_kamar = getKamar();
$list_tamu = getTamu();
$data = null;

if (count($_POST) > 0) {
    if (isset($_REQUEST['id'])) {
        $id = $_REQUEST['id'];
        $tgl_checkout = $_REQUEST['tgl_checkout'];

        // Update
        if (!($stmt = $mysqli->prepare("UPDATE tbl_transaksi SET tgl_checkout=?, status='selesai', jml_bayar=? WHERE id=?"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $res = $mysqli->query("SELECT id_kamar FROM tbl_transaksi WHERE id = '$id'");
        $row = $res->fetch_object();
        $jml_bayar = calculateHarga($id, $tgl_checkout);

        ubahStatusKamar($row->id_kamar, 'kosong');

        $stmt->bind_param('sii', $tgl_checkout, $jml_bayar, $id);
        if ($stmt->execute()) {
            redirectTo("list_transaksi.php");
        }
    } else {
        $id_kamar = $_REQUEST['id_kamar'];
        $id_pengunjung = $_REQUEST['id_pengunjung'];
        $tgl_checkin = $_REQUEST['tgl_checkin'];

        // Insert
        if (!($stmt = $mysqli->prepare("INSERT INTO tbl_transaksi (id_kamar,id_pengunjung,tgl_checkin,status) VALUES (?,?,?, 'aktif')"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('iss', $id_kamar, $id_pengunjung, $tgl_checkin);
        if ($stmt->execute()) {
            ubahStatusKamar($id_kamar, 'digunakan');
            redirectTo("list_transaksi.php");
        }
    }
}

if (isset($_REQUEST['id'])) {
    // Get data and place into input
    $id = $_REQUEST['id'];
    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_transaksi where id = ? LIMIT 1"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    $stmt->bind_param('i', $id);
    if ($stmt->execute()) {
        $res = $stmt->get_result();
        $data = $res->fetch_object();
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Form Transaksi</h3>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="row">
            <div class="input-field col s12">
                <select name="id_kamar" required <?= (!$data) ?: 'disabled' ?>>
                    <option value="" disabled selected>Choose your option</option>
                    <?php foreach ($list_kamar as $value) { ?>
                        <option <?= (!($data && $data->id_kamar == $value->id) ?: "selected") ?> value="<?= $value->id ?>"><?= $value->nomor_kamar ?></option>
                    <?php } ?>
                </select>
                <label>Kamar</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s9">
                <select name="id_pengunjung" required <?= (!$data) ?: 'disabled' ?>>
                    <option value="" disabled selected>Choose your option</option>
                    <?php foreach ($list_tamu as $value) { ?>
                        <option <?= (!($data && $data->id_pengunjung == $value->id) ?: "selected") ?> value="<?= $value->id ?>">(<?= $value->id ?>) <?= $value->nama_lengkap ?></option>
                    <?php } ?>
                </select>
                <label>Tamu</label>
            </div>
            <?php if (!$data) { ?>
                <div class="input-field col s3">
                    <a class="waves-effect waves-light btn" href="/form_pengunjung.php">
                        <i class="material-icons left">create</i>
                        Tambah
                    </a>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <input id="tgl_checkin" type="date" class="validate" required name="tgl_checkin" <?= (!$data) ?: 'disabled' ?> <?= (!$data) ?: "value='$data->tgl_checkin'" ?>>
                <label for="tgl_checkin">Tanggal CheckIn</label>
            </div>
            <div class="input-field col s6">
                <input id="tgl_checkout" type="date" class="validate" name="tgl_checkout" <?= ($data) ? '' : 'disabled' ?> <?= (!($data && $data->tgl_checkout != null)) ?: "value='$data->tgl_checkout'" ?>>
                <label for="tgl_checkout">Tanggal CheckOut</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    Simpan
                    <i class="material-icons right">send</i>
                </button>
                <a class="btn waves-effect waves-light red" href="/list_transaksi.php">
                    Kembali ke Daftar
                    <i class="material-icons right">reply</i>
                </a>
            </div>
        </div>
    </form>
</div>
<?php
include('views/footer.php');
?>