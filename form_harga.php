<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

if (count($_POST) > 0) {
    $nama_paket = $_REQUEST['nama_paket'];
    $harga = $_REQUEST['harga'];

    if (isset($_REQUEST['id'])) {
        $id = $_REQUEST['id'];

        // Update
        if (!($stmt = $mysqli->prepare("UPDATE tbl_harga SET nama_paket=?, harga=? WHERE id=?"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('sii', $nama_paket, $harga, $id);
        if ($stmt->execute()) {
            redirectTo("form_harga.php?id=$id");
        }
    } else {
        // Insert
        if (!($stmt = $mysqli->prepare("INSERT INTO tbl_harga (nama_paket,harga) VALUES (?, ?)"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('si', $nama_paket, $harga);
        if ($stmt->execute()) {
            $insert_id = $mysqli->insert_id;
            redirectTo("form_harga.php?id=$insert_id");
        }
    }
}

$data = null;

if (isset($_REQUEST['id'])) {
    // Get data and place into input
    $id = $_REQUEST['id'];
    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_harga where id = ? LIMIT 1"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    $stmt->bind_param('i', $id);
    if ($stmt->execute()) {
        $res = $stmt->get_result();
        $data = $res->fetch_object();
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Form Harga</h3>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="row">
            <div class="input-field col s12">
                <input id="nama" type="text" class="validate" name="nama_paket" <?= (!$data) ?: "value='$data->nama_paket'" ?>>
                <label for="nama">Nama Paket Harga</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="harga" type="number" class="validate" name="harga" <?= (!$data) ?: "value='$data->harga'" ?>>
                <label for="harga">Harga</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    Simpan
                    <i class="material-icons right">send</i>
                </button>
                <a class="btn waves-effect waves-light red" href="/list_harga.php">
                    Kembali ke Daftar
                    <i class="material-icons right">reply</i>
                </a>
            </div>
        </div>
    </form>
</div>
<?php
include('views/footer.php');
?>