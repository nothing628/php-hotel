<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$id = $_REQUEST['id'];

if ($id) {
    if (!($stmt = $mysqli->prepare("DELETE FROM tbl_karyawan WHERE id = ?"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    $stmt->bind_param('i', $id);
    $stmt->execute();
}

redirectTo("list_admin.php");