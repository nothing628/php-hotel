<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$data = [];
$query = "SELECT * FROM tbl_karyawan";
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;

if ($keyword) {
    $query .= " WHERE username LIKE ?";
}

if (!($stmt = $mysqli->prepare($query))) {
    die("Prepare failed: ($mysqli->errno) $mysqli->error");
}

if ($keyword) {
    $new_keyword = "%$keyword%";
    $stmt->bind_param('s', $new_keyword);
}

if ($stmt->execute()) {
    $res = $stmt->get_result();

    while ($row = $res->fetch_object()) {
        $data[] = $row;
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Daftar Karyawan</h3>
    <form action="" method="get">
        <div class="row">
            <div class="input-field col s8">
                <input placeholder="Pencarian" id="keyword" type="text" class="validate" name="keyword" <?= (!$keyword) ?: "value='$keyword'" ?>>
                <label for="keyword">Pencarian</label>
            </div>
            <div class="col s4">
                <button class="waves-effect waves-light btn" type="submit">
                    <i class="material-icons left">search</i>
                    Cari
                </button>
                <a class="waves-effect waves-light btn" href="/form_admin.php">
                    <i class="material-icons left">create</i>
                    Tambah
                </a>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col s12">
            <table class="responsive-table striped highlight">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Karyawan</th>
                        <th>Aktif</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->username ?></td>
                            <td><?= $value->is_active == 1 ? "Aktif" : "Non-Aktif" ?></td>
                            <td>
                                <a class="waves-effect waves-light btn btn-floating blue" href="/form_admin.php?id=<?= $value->id ?>">
                                    <i class="material-icons left">create</i>
                                </a>
                                <a class="waves-effect waves-light btn btn-floating red" href="/delete_admin.php?id=<?= $value->id ?>">
                                    <i class="material-icons left">delete_forever</i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('views/footer.php');
?>