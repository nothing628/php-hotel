<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$data = [];
$query = "SELECT tbl_kamar.*, tbl_harga.nama_paket, tbl_harga.harga FROM tbl_kamar JOIN tbl_harga ON tbl_harga.id = tbl_kamar.id_harga";
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;

if ($keyword) {
    $query .= " WHERE nomor_kamar LIKE ?";
}

if (!($stmt = $mysqli->prepare($query))) {
    die("Prepare failed: ($mysqli->errno) $mysqli->error");
}

if ($keyword) {
    $new_keyword = "%$keyword%";
    $stmt->bind_param('s', $new_keyword);
}

if ($stmt->execute()) {
    $res = $stmt->get_result();

    while ($row = $res->fetch_object()) {
        $data[] = $row;
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Daftar Kamar</h3>
    <form action="" method="get">
        <div class="row">
            <div class="input-field col s8">
                <input placeholder="Pencarian" id="keyword" type="text" class="validate" name="keyword" <?= (!$keyword) ?: "value='$keyword'" ?>>
                <label for="keyword">Pencarian</label>
            </div>
            <div class="col s4">
                <button class="waves-effect waves-light btn" type="submit">
                    <i class="material-icons left">search</i>
                    Cari
                </button>
                <a class="waves-effect waves-light btn" href="/form_kamar.php">
                    <i class="material-icons left">create</i>
                    Tambah
                </a>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col s12">
            <table class="responsive-table striped highlight">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nomor Kamar</th>
                        <th>Harga</th>
                        <th>Aktif</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->nomor_kamar ?></td>
                            <td><?= $value->nama_paket ?> (Rp<?= $value->harga ?>)</td>
                            <td><?= $value->kamar_aktif == 1 ? 'Aktif' : 'Non-Aktif' ?></td>
                            <td><?= $value->status ?></td>
                            <td>
                                <a class="waves-effect waves-light btn btn-floating blue" href="/form_kamar.php?id=<?= $value->id ?>">
                                    <i class="material-icons left">create</i>
                                </a>
                                <a class="waves-effect waves-light btn btn-floating red" href="/delete_kamar.php?id=<?= $value->id ?>">
                                    <i class="material-icons left">delete_forever</i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('views/footer.php');
?>