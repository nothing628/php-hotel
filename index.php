<?php

require_once('connection.php');

if (isset($_SESSION['user_id'])) {
    header('Location: /admin.php', true, 302);
    die();
}

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $query = "SELECT * FROM tbl_karyawan WHERE username = '$username' AND password= '$password' LIMIT 1";

    try {
        $result = $mysqli->query($query);

        if ($row = $result->fetch_assoc()) {
            $_SESSION['user_id'] = $row['id'];
            $_SESSION['user'] = $row;

            header('Location: /admin.php', true, 302);
            die();
        } else {
            $error[] = "Karyawan tidak ditemukan!";
        }
    } catch (\Exception $ex) {
        $error[] = $ex->getMessage();
        die($ex->getMessage());
    }
}

include('views/header.php');
?>
<div class="container">
    <div class="section-login">
        <div class="row">
            <div class="col s12 l12">
                <form method="post" action="">
                    <h3>Harap Login</h3>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="User Name" id="username" type="text" class="validate" name="username">
                            <label for="username">User Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Password" id="password" type="password" class="validate" name="password">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <button class="btn waves-effect waves-light" type="submit" name="action">Masuk ke Aplikasi
                        <i class="material-icons right">send</i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include('views/copyright.php');
include('views/footer.php');
