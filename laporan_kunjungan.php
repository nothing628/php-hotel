<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$total = 0;
$data = [];
$query = "SELECT tbl_transaksi.*, tbl_kamar.nomor_kamar, tbl_pengunjung.nama_lengkap FROM tbl_transaksi";
$query .= " JOIN tbl_kamar ON tbl_transaksi.id_kamar = tbl_kamar.id";
$query .= " JOIN tbl_pengunjung ON tbl_transaksi.id_pengunjung = tbl_pengunjung.id";
$query .= " WHERE tbl_transaksi.status = 'selesai'";

if (!($stmt = $mysqli->prepare($query))) {
    die("Prepare failed: ($mysqli->errno) $mysqli->error");
}

if ($stmt->execute()) {
    $res = $stmt->get_result();

    while ($row = $res->fetch_object()) {
        $data[] = $row;
        $total += intval($row->jml_bayar);
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Laporan Kunjungan</h3>
    <div class="row">
        <div class="col s12">
            <table class="responsive-table striped highlight">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nomor Kamar</th>
                        <th>Nama Lengkap</th>
                        <th>Status</th>
                        <th>Jumlah Bayar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->nomor_kamar ?></td>
                            <td>(<?= $value->id_pengunjung ?>) <?= $value->nama_lengkap ?></td>
                            <td><?= $value->status ?></td>
                            <td>Rp<?= $value->jml_bayar ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">Total Transaksi Penyewaan</td>
                        <td>Rp<?= $total ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<?php
include('views/footer.php');
?>