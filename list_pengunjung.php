<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

$data = [];
$query = "SELECT * FROM tbl_pengunjung";
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;

if ($keyword) {
    $query .= " WHERE nama_lengkap LIKE ? OR id LIKE ?";
}

if (!($stmt = $mysqli->prepare($query))) {
    die("Prepare failed: ($mysqli->errno) $mysqli->error");
}

if ($keyword) {
    $new_keyword = "%$keyword%";
    $stmt->bind_param('ss', $new_keyword, $new_keyword);
}

if ($stmt->execute()) {
    $res = $stmt->get_result();

    while ($row = $res->fetch_object()) {
        $data[] = $row;
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Daftar Pengunjung</h3>
    <form action="" method="get">
        <div class="row">
            <div class="input-field col s8">
                <input placeholder="Pencarian" id="keyword" type="text" class="validate" name="keyword" <?= (!$keyword) ?: "value='$keyword'" ?>>
                <label for="keyword">Pencarian</label>
            </div>
            <div class="col s4">
                <button class="waves-effect waves-light btn" type="submit">
                    <i class="material-icons left">search</i>
                    Cari
                </button>
                <a class="waves-effect waves-light btn" href="/form_pengunjung.php">
                    <i class="material-icons left">create</i>
                    Tambah
                </a>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col s12">
            <table class="responsive-table striped highlight">
                <thead>
                    <tr>
                        <th>Ktp</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th>Tgl Lahir</th>
                        <th>Diblokir</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->nama_lengkap ?></td>
                            <td><?= $value->jenis_kelamin ?></td>
                            <td><?= $value->tgl_lahir ?></td>
                            <td><?= $value->is_blocked == 1 ? "Diblokir" : "" ?></td>
                            <td>
                                <a class="waves-effect waves-light btn btn-floating blue" href="/form_pengunjung.php?id=<?= $value->id ?>">
                                    <i class="material-icons left">create</i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('views/footer.php');
?>