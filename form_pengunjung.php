<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

if (count($_POST) > 0) {
    $nama_lengkap = $_REQUEST['nama_lengkap'];
    $jenis_kelamin = $_REQUEST['jenis_kelamin'];
    $tgl_lahir = $_REQUEST['tgl_lahir'];
    $is_blocked = isset($_REQUEST['is_blocked']);

    if (isset($_REQUEST['id'])) {
        $id = $_REQUEST['id'];

        // Update
        if (!($stmt = $mysqli->prepare("UPDATE tbl_pengunjung SET nama_lengkap=?, jenis_kelamin=?, tgl_lahir=?, is_blocked=? WHERE id=?"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('sssis', $nama_lengkap, $jenis_kelamin, $tgl_lahir, $is_blocked, $id);
        if ($stmt->execute()) {
            redirectTo("form_pengunjung.php?id=$id");
        }
    } else {
        // Insert
        $ktp = $_REQUEST['ktp'];
        if (!($stmt = $mysqli->prepare("INSERT INTO tbl_pengunjung (id,nama_lengkap,jenis_kelamin,tgl_lahir,is_blocked) VALUES (?,?,?,?,?)"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('ssssi', $ktp, $nama_lengkap, $jenis_kelamin, $tgl_lahir, $is_blocked);
        if ($stmt->execute()) {
            redirectTo("form_pengunjung.php?id=$ktp");
        }
    }
}

$data = null;

if (isset($_REQUEST['id'])) {
    // Get data and place into input
    $id = $_REQUEST['id'];
    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_pengunjung where id = ? LIMIT 1"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    $stmt->bind_param('i', $id);
    if ($stmt->execute()) {
        $res = $stmt->get_result();
        $data = $res->fetch_object();
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Form Pengunjung</h3>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="row">
            <div class="input-field col s12">
                <input id="ktp" type="text" class="validate" name="ktp" maxlength="30" required <?= (!$data) ?: "value='$data->id' disabled" ?>>
                <label for="ktp">ID/KTP</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="nama_lengkap" type="text" class="validate" name="nama_lengkap" required <?= (!$data) ?: "value='$data->nama_lengkap'" ?>>
                <label for="nama_lengkap">Nama Lengkap</label>
            </div>
        </div>
        <div class="row">
            <div class="col s4">
                <label>Jenis Kelamin</label>
            </div>
            <div class="col s4">
                <label>
                    <input class="with-gap" name="jenis_kelamin" type="radio" value="Laki-Laki" <?= (!($data && $data->jenis_kelamin == 'Laki-Laki')) ?: "checked" ?> />
                    <span>Laki-Laki</span>
                </label>
            </div>
            <div class="col s4">
                <label>
                    <input class="with-gap" name="jenis_kelamin" type="radio" value="Perempuan" <?= (!($data && $data->jenis_kelamin == 'Perempuan')) ?: "checked" ?> />
                    <span>Perempuan</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="nomor_kamar" type="date" class="validate" name="tgl_lahir" <?= (!$data) ?: "value='$data->tgl_lahir'" ?>>
                <label for="nomor_kamar">Tanggal Lahir</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label>
                    <input type="checkbox" class="filled-in" <?= (!$data) ?: $data->is_blocked != 1 ?: "checked='checked'" ?> name="is_blocked" value="1" />
                    <span>Blokir Tamu</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    Simpan
                    <i class="material-icons right">send</i>
                </button>
                <a class="btn waves-effect waves-light red" href="/list_pengunjung.php">
                    Kembali ke Daftar
                    <i class="material-icons right">reply</i>
                </a>
            </div>
        </div>
    </form>
</div>
<?php
include('views/footer.php');
?>