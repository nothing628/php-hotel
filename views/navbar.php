<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Logo</a>


        <ul class="right hide-on-med-and-down">
            <?php if (isset($_SESSION['user_id'])) { ?>
                <li>
                    <a class="dropdown-trigger" href="#!" data-target="dropdown1">
                        Master
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                    <ul id="dropdown1" class="dropdown-content">
                        <li><a href="list_harga.php">Master Harga</a></li>
                        <li><a href="list_kamar.php">Master Kamar</a></li>
                        <li><a href="list_admin.php">Master Karyawan</a></li>
                        <li><a href="list_pengunjung.php">Master Tamu</a></li>
                    </ul>
                </li>
                <li><a href="list_transaksi.php">Transaksi</a></li>
                <li><a href="laporan_kunjungan.php">Laporan Kunjungan</a></li>
                <li><a href="logout.php">Keluar</a></li>
            <?php } else { ?>
                <li><a href="index.php">Login</a></li>
            <?php } ?>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <?php if (isset($_SESSION['user_id'])) { ?>
                <li>
                    <a class="dropdown-trigger" href="#!" data-target="dropdown2">
                        Master
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                    <ul id="dropdown2" class="dropdown-content">
                        <li><a href="list_harga.php">Master Harga</a></li>
                        <li><a href="list_kamar.php">Master Kamar</a></li>
                        <li><a href="list_admin.php">Master Karyawan</a></li>
                        <li><a href="list_pengunjung.php">Master Tamu</a></li>
                    </ul>
                </li>
                <li><a href="list_transaksi.php">Transaksi</a></li>
                <li><a href="laporan_kunjungan.php">Laporan Kunjungan</a></li>
                <li><a href="logout.php">Keluar</a></li>
            <?php } else { ?>
                <li><a href="index.php">Login</a></li>
            <?php } ?>
        </ul>

        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>

    </div>
</nav>