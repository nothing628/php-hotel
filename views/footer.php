<script src="/public/js/jquery-3.5.1.min.js"></script>
<script src="/public/js/materialize.min.js"></script>
<script>
    (function($) {
        $(function() {

            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('select').formSelect();

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
</body>

</html>