<?php

require_once('connection.php');
require_once('helper.php');

checkLogin();

if (count($_POST) > 0) {
    $username = $_REQUEST['username'];
    $password = !isset($_REQUEST['password']) ?: $_REQUEST['password'];
    $is_active = isset($_REQUEST['is_active']);

    if (isset($_REQUEST['id'])) {
        $id = $_REQUEST['id'];

        // Update
        if (!($stmt = $mysqli->prepare("UPDATE tbl_karyawan SET username=?, is_active=? WHERE id=?"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('sii', $username, $is_active, $id);
        if ($stmt->execute()) {
            redirectTo("form_admin.php?id=$id");
        }
    } else {
        // Insert
        if (!($stmt = $mysqli->prepare("INSERT INTO tbl_karyawan (username,password,level, is_active) VALUES (?, ?, 'karyawan', ?)"))) {
            die("Prepare failed: ($mysqli->errno) $mysqli->error");
        }

        $stmt->bind_param('ssi', $username, $password, $is_active);
        if ($stmt->execute()) {
            $insert_id = $mysqli->insert_id;
            redirectTo("form_admin.php?id=$insert_id");
        }
    }
}

$data = null;

if (isset($_REQUEST['id'])) {
    // Get data and place into input
    $id = $_REQUEST['id'];
    if (!($stmt = $mysqli->prepare("SELECT * FROM tbl_karyawan where id = ? LIMIT 1"))) {
        die("Prepare failed: ($mysqli->errno) $mysqli->error");
    }

    $stmt->bind_param('i', $id);
    if ($stmt->execute()) {
        $res = $stmt->get_result();
        $data = $res->fetch_object();
    }
}

include('views/header.php');
?>
<div class="container" id="app">
    <h3>Form Admin</h3>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="row">
            <div class="input-field col s6">
                <input id="username" type="text" class="validate" name="username" required <?= (!$data) ?: "value='$data->username'" ?>>
                <label for="username">Nama Karyawan</label>
            </div>
            <div class="input-field col s6">
                <input id="password" type="password" class="validate" name="password" required <?= (!$data) ?: "disabled value='aaaaaa'" ?>>
                <label for="password">Kata Sandi</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label>
                    <input type="checkbox" class="filled-in" <?= (!$data) ?: $data->is_active != 1 ?: "checked='checked'" ?> value="1" name="is_active" />
                    <span>Aktifkan Akun</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    Simpan
                    <i class="material-icons right">send</i>
                </button>
                <a class="btn waves-effect waves-light red" href="/list_admin.php">
                    Kembali ke Daftar
                    <i class="material-icons right">reply</i>
                </a>
            </div>
        </div>
    </form>
</div>
<?php
include('views/footer.php');
?>