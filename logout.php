<?php

require_once('connection.php');
require_once('helper.php');

session_destroy();
redirectTo('index.php');
